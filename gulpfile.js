// Adiciona os modulos instalados
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');

// Funçao para compilar o SASS e adicionar os prefixos
function compilaSass() {
  return gulp
    .src('./src/styles/scss/*.scss')
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('./src/styles/css/'))
    .pipe(browserSync.stream());
};

// Tarefa de gulp para a função de SASS
gulp.task('sass', compilaSass);

// Função para juntar o JS
function gulpJs() {
  return gulp.src('./src/javascript/main/*.js')
    .pipe(concat('script.js'))
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(uglify())
    .pipe(gulp.dest('./src/javascript/'))
    .pipe(browserSync.stream())
};

gulp.task('mainjs', gulpJs)

// Função para iniciar o browser
function browser() {
  browserSync.init({
    server: {
      baseDir: "./src"
    }
  });
};;;

// Tarefa para iniciar o browser-sync
gulp.task('browser-sync', browser);

// Função de watch do Gulp
function watch() {
  gulp.watch('./src/styles/scss/*/*.scss', compilaSass);
  gulp.watch('./src/javascript/main/*.js', gulpJs);
  gulp.watch(['./src/*.html']).on('change', browserSync.reload);
};

// Inicia a tarefa de watch
gulp.task('watch', watch);

// Tarefa padrão do Gulp, que inicia o watch e o browser-sync
gulp.task('default', gulp.parallel('watch', 'browser-sync', 'sass', 'mainjs'));