let id = 0;

function idGenerator() {
  return id++;
}

class CoursesController {
  constructor() {
    this.coursesShopping = [
      {
        id: idGenerator(),
        title: `Curso_${id}`,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        imageUrl: './assets/images/course.jpg',
        acquired: false
      },
      {
        id: idGenerator(),
        title: `Curso_${id}`,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        imageUrl: './assets/images/course.jpg',
        acquired: false
      },
    ]

    this.coursesUser = [
      {
        id: idGenerator(),
        title: `Curso_${id}`,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        imageUrl: './assets/images/course.jpg',
        acquired: true
      },
      {
        id: idGenerator(),
        title: `Curso_${id}`,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        imageUrl: './assets/images/course.jpg',
        acquired: true
      },
      {
        id: idGenerator(),
        title: `Curso_${id}`,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        imageUrl: './assets/images/course.jpg',
        acquired: true
      },
      {
        id: idGenerator(),
        title: `Curso_${id}`,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        imageUrl: './assets/images/course.jpg',
        acquired: true
      },
      {
        id: idGenerator(),
        title: `Curso_${id}`,
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
        imageUrl: './assets/images/course.jpg',
        acquired: true
      },
    ];

    this.view = new CoursesView();
    this.view.updateScreenAreaCourse(this.coursesUser)
    this.view.updateScreenAreaAddCourse(this.coursesShopping);

    this.$boxModal = document.querySelector('.modal_box');
    this.$modal = document.querySelector('.modal_container');
    this.onLoadModal();

    this.$modal_course = document.querySelector('.modal_course_container');
    this.$boxModal_course = document.querySelector('.modal_box_course');
  }

  addCourse(id) {
    let index = this.coursesShopping.findIndex(course => course.id === id);
    let selectCourse = this.coursesShopping.find(course => course.id === id);
    let courseAcquired = {
      ...selectCourse,
      acquired: true
    }
    this.coursesUser.push(courseAcquired);
    this.coursesShopping.splice(index, 1);
    this.view.updateScreenAreaAddCourse(this.coursesShopping);
    this.view.updateScreenAreaCourse(this.coursesUser);
    if (this.coursesShopping.length === 0) this.closeModal('event', 'addCourse')
  }

  isAcquired() {

  }

  openAddCourse() {
    this.$modal_course.classList.add('active');
    this.$boxModal_course.classList.add('active');
  }

  closeModal(event, type) {
    if (type === 'featured')
      this.$modal.classList.remove('active');
    if (type === 'addCourse')
      this.$modal_course.classList.remove('active');
  }

  clickOut({ target, currentTarget }, type) {
    if (type === 'featured')
      if (target === currentTarget) this.$modal.classList.remove('active');

    if (type === 'addCourse')
      if (target === currentTarget) this.$modal_course.classList.remove('active');
  }

  onLoadModal() {
    this.$modal.classList.add('active');
    this.$boxModal.classList.add('active');
  }

}