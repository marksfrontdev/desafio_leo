class CoursesView {
  constructor() {
    this.$courses = document.querySelector('#courses-home');
    this.$coursesAddModal = document.querySelector('.modal_box_course');
  }

  addCourse() {
    return `
    <div class="course-add-container" onclick="App.openAddCourse()" >
      <div class="course-add" >
        <img class="course-add-img" src="./assets/images/course.svg" alt="Adicionar Curso"/>
        <div class="course-add-detail">
          <p>Adicionar</p>
          <p class="course-title">Curso</p>
        </div>
      </div>
    </div>
    `
  }

  templateAreaCourse(model) {

    let coursesModel = model.map(course => {
      return `<div class="course-item" >
              <img class="course-img" src="${course.imageUrl}" alt="${course.title}"/>
              <div class="course-detail">
                <h2 class="course-title">${course.title}</h2>
                <p class="course-description">${course.description}</p>
                <button class="course-btn">Ver Curso</button>
              </div>
            </div>`
    }).join('');

    return coursesModel.concat(this.addCourse())
  }

  templateAreaAddCourse(model) {

    return `
    <ul class="modal_box_course">
      <button onclick="App.closeModal(event, 'addCourse')" class="modal_course_close"><img src="./assets/images/close.svg" alt="fechar"></button>
      ${model.length ? model.map(course => {
      return ` 
          <li class="modal_couse_item_container">
            <div class="modal_course_img_container">
              <img src="${course.imageUrl}" alt="${course.title}"">
            </div>
            <div class="modal_course_detail_container">
              <h3 class="modal_course_title">${course.title}</h3>
              <p class="modal_course_description">${course.description}</p>
              <button class="modal_course_btn" onclick="App.addCourse(${course.id})">Adicionar Curso</button>
            </div>
          </li>`
    }).join('') : '<li>Agradecemos por ter adquirido nossos cursos</li>'}
    </ul>
    `

  }

  updateScreenAreaCourse(model) {
    this.$courses.innerHTML = this.templateAreaCourse(model);
  }

  updateScreenAreaAddCourse(model) {
    this.$coursesAddModal.innerHTML = this.templateAreaAddCourse(model);
  }
}